export class InfoMsg {
    msgTypeArr = ['INFO', 'WARNING', 'ERROR'];
    private MSG_TYPES = [1,2,3];        
    type;
    msgType;
    title;
    discription;
    okFunc;    
    constructor (type: number, title: String, discription: String, func: Function){
        if (this.MSG_TYPES.includes(type)) {
        this.type = type;
        this.msgType = this.msgTypeArr[type-1];
        this.title = title;
        this.discription = discription; 
        this.okFunc = func;       
        }
    }
}