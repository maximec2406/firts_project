import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'taskExecutor'
})
export class taskExecutorPipe implements PipeTransform {
    transform(users, value) {
        if (!!users && !!value){
        return users.filter(user => {
            return (user.id != value)
        })
        } else {
            return users;
        }
    }
}