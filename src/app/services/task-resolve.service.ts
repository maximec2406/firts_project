import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';  
import { CacheService } from './cache.service';


  @Injectable()
  export class TaskStartResolver implements Resolve<any> {
    taskId;
    constructor(private cacheService: CacheService, private router: Router) {}

    resolve(
      route: ActivatedRouteSnapshot, 
      state: RouterStateSnapshot): Observable<any> | Promise<any> | any { 
        this.taskId = route.paramMap.get('taskId');                    
        if (this.taskId && this.taskId !="new"){
            return this.cacheService.updateTask(this.taskId);                  
        } else {
            return true;
        }
      }
  }