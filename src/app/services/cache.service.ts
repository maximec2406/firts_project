import { Injectable } from '@angular/core';
import { MsgService } from './msg.servise';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class CacheService {
    userCollection;
    projectCollection;
    taskCollection;
    userProjectCollection;
    projectSprintCollection;
    task;
    //private serverUrl = 'http://167.99.250.211:8000/api/';
    private serverUrl = 'http://localhost:5000/api/';

    constructor ( private http: HttpClient, private ms: MsgService) {

    }

    ngOnInit(){
        this.http.get(this.serverUrl + 'projects', {headers: this.getHeaders()}).subscribe( data => { this.projectCollection = data});
    }

    started(){        
        if (!this.projectCollection || !this.userCollection || !this.taskCollection || !this.getProjectCollection || this.getProjectSprintCollection){
            return this.updateProjectCollection()
            .then(() => this.updateUserCollection()
            .then(() => this.updateTaskCollection()
            .then(() => this.updateProjectSprintCollection())
            )
        )    
        } else {
            return Promise.resolve();
        }
    }
    
    getHeaders(){
        return new HttpHeaders({'Authorization':'Bearer ' + localStorage.getItem("tfs18")});
    }

    getUserCollection(){
        return (this.userCollection);
    }
    updateUserCollection(){
        return this.http.get(this.serverUrl + 'users', {headers: this.getHeaders()})
        .toPromise()
        .then(data => { this.userCollection = data; return Promise.resolve()})
        .catch(err => {return Promise.reject(err.message || 'Server error');})
    }

    getProjectCollection(){
        return (this.projectCollection);
    }
    updateProjectCollection(){
        return this.http.get(this.serverUrl + 'projects', {headers: this.getHeaders()})
        .toPromise()
        .then(data => { this.projectCollection = data; return Promise.resolve()})
        .catch(err => {return Promise.reject(err.message || 'Server error');})
    }
    getTaskCollection(){
        return (this.taskCollection);
    }    
    updateTaskCollection(){
        return this.http.get(this.serverUrl + 'tasks', {headers: this.getHeaders()})
        .toPromise()
        .then(data => { this.taskCollection = data; return Promise.resolve()})
        .catch(err => {return Promise.reject(err.message || 'Server error');});    
    }

    getUserProjectCollection(userId) {
        return this.userProjectCollection;
    }
    updateUserProjectCollection(userId){
        return this.http.get(this.serverUrl + "users/"+ userId +"/projects",  
        {headers: this.getHeaders()})
        .toPromise()
        .then( (data:any) => { 
            this.userProjectCollection = data.map( d => { 
            return { 
              "project" : {
                "id" : d.project.id,
                "name" :d.project.name,
                "workTimeSec" : d.workTimeSec
            }
          }
        })})
        .catch(err => { return Promise.reject(err.message || 'Server error');
        });
      }
    getProjectSprintCollection(){
        return this.projectSprintCollection; 
    }
    updateProjectSprintCollection(){
        return this.http.get(this.serverUrl + "sprints",  
        {headers: this.getHeaders()})
        .toPromise()
        .then( (data:any) => { 
            this.projectSprintCollection = data
            .filter(d => { return !d.closeAt })
            .map( d => { 
                return { 
                    "projectId" : d.projectId,
                    "projectName" : this.getProjectNameById(d.projectId),
                    "openAt" : d.openAt,
                    "sprintId" : d.id                
                }
            }
        )})
        .catch(err => { return Promise.reject(err.message || 'Server error');
        });
    }
    getProjectNameById(projectId){
         return this.projectCollection.filter(p => { return p.id === projectId })[0].name;
    }

    getTask(){
        console.log(this.task);
        return this.task;
    }
    updateTask(taskId) {
        return this.http.get(this.serverUrl + "tasks/"+ taskId,  
        {headers: this.getHeaders()})
        .toPromise()
        .then(data => { this.task = data;})
        .catch(err => { console.log('ytn')});       
    }
    
 }

