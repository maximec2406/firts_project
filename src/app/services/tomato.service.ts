import { Injectable, Input } from '@angular/core';
import { MsgService } from './msg.servise';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class tomatoService {
    task;

    getTask(){
        return this.task;
    }

    setTask(task){
        this.task = task;
    }
}