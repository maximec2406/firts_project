import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TomatoSettingsPageComponent } from './tomato-settings-page.component';

describe('TomatoSettingsPageComponent', () => {
  let component: TomatoSettingsPageComponent;
  let fixture: ComponentFixture<TomatoSettingsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TomatoSettingsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TomatoSettingsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
