import { Component, OnInit } from '@angular/core';
import { PostService } from '../../../services/post.service';
import { InfoMsg } from '../../../model/msg.model';
import { MsgService } from '../../../services/msg.servise';
import { ActivatedRoute } from '@angular/router';
import { Task } from '../../../model/task.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../../model/user.model';
import { CacheService } from '../../../services/cache.service';
import { TaskStartResolver } from '../../../services/task-resolve.service';

@Component({
  selector: 'app-task-settings-page',
  templateUrl: './task-settings-page.component.html',
  styleUrls: ['./task-settings-page.component.less'],
})
export class TaskSettingsComponent implements OnInit {

  taskCollection;
  projectId;
  taskId;  
  msg;   
  task; 
  form: FormGroup;
  userId;
  allUsers;
  allUsers2;
  me
  tr :TaskStartResolver;
  
  constructor(private postService: PostService,
    private msgServise: MsgService,
    private route: ActivatedRoute,
    private cacheService: CacheService,
    private fb: FormBuilder) { }
    
  ngOnInit() {  
    this.taskCollection = this.postService.getTasks();
    this.postService.getMe().subscribe(data => this.me=data); 
    this.postService.getAllUsers().subscribe(users => { this.allUsers = users; this.allUsers2 = users })   
    this.postService.getMe().subscribe((data:User) => this.userId = data.id)
    this.route.paramMap.subscribe(params => {          
      this.projectId = params.get('projectId');
      this.taskId = params.get('taskId'); ;     
    });    
    if (!!this.taskId){
      if (this.taskId == "new") {
        this.task = new Task();
      } else {
       this.task = this.cacheService.getTask()        
      }  
      this.form = this.createForm();  
    } else {            
      this.taskCollection = this.postService.getTasks();                 
    }     
  }  
  createTask(task) {        
    task.creatorId = this.me.id;
    task.creatorUserName = this.me.userName;
    task.executorId = this.getId(task.executor);    
    this.postService.createTask(task);
  }   
  saveTask(task) {  
    task.creatorId = this.task.creator.id;
    task.creatorUserName = this.task.creator.userName; 
    task.executorId = this.getId(task.executor)  
    this.postService.saveTask(task, this.taskId);
  } 

  getId(name) {
    return this.allUsers2.filter(user => {return user.userName == name})[0].id;
  }
  

  private createForm(): FormGroup {
    return this.fb.group({             
      subject : [this.task.subject],
      description : [this.task.description],
      status : [this.task.status || "new"],
      priority : [this.task.priority || "normal"],
      sprintId  : [this.task.sprintId],
      projectId : [this.projectId],
      estimateTimeSec : [this.task.estimateTimeSec || 0],      
      executor : [this.task.executor.userName || ""],
      creator : [this.task.creator.userName]      
    });
  }
}
