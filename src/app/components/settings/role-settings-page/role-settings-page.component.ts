import { Component, OnInit } from '@angular/core';
import { PostService } from '../../../services/post.service';

@Component({
  selector: 'app-role-settings-page',
  templateUrl: './role-settings-page.component.html',
  styleUrls: ['./role-settings-page.component.less']
})
export class RoleSettingsPageComponent implements OnInit {

  projectSprintCollection;
  psc;
  constructor(private postService: PostService) { }

  ngOnInit() {
    this.projectSprintCollection = this.postService.getProjectSprintCollection();    
  }

}
