import { Component, Input } from '@angular/core';
import { UsersService } from './users.service';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],  
})
export class AppComponent { 
  
  isAuth;

  constructor (private route: Router) {}
 
  ngOnInit(){
    localStorage.getItem('tfs18') ? this.isAuth = true : this.isAuth = false;
    this.route.events.subscribe((event:any) => { event.url === '/login' ? this.isAuth = false : this.isAuth = true });        
  }

  showDroprown() {
    document.querySelector('#dropdown1').setAttribute('style','display: block') ;   
  }
  
  hiddenDropDown(){
    document.querySelector('#dropdown1').setAttribute('style','display: none');
  }

}
